const initialState = {
  data: [],
  isLoading: false,
  error: false,
}

export function booksReducer(state = initialState, action) {
  switch (action.type) {
    
    case 'GET_DATA_REQUEST':
      return { ...state, data: action.payload, isFetching: true, error: '' }

    case 'GET_DATA_SUCCESS':
      return { ...state, data: action.payload, isFetching: false, error: '' }

    case 'GET_DATA_ERROR':
      return { ...state, error: action.payload.message, isFetching: false }

    default:
      return state
  }
}