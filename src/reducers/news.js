const initialState = {
  data: [],
  isFetching: false,
  error: false,
}

export function newsReducer(state = initialState, action) {
  switch (action.type) {
    
    case 'GET_NEWS_DATA_REQUEST':
      return { 
        ...state, 
        isFetching: true, 
        error: '' 
      }

    case 'GET_NEWS_DATA_SUCCESS':
      return { 
        ...state, 
        data: action.payload, 
        isFetching: false, 
        error: '' 
      }

    case 'GET_NEWS_DATA_ERROR':
      return { 
        ...state, 
        error: action.payload.message, 
        isFetching: false 
      }

    default:
      return state
  }
}