import { combineReducers } from 'redux'
import { booksReducer } from './books'
import { userReducer } from './user'
import { newsReducer } from './news'


export const rootReducer = combineReducers({
	books: booksReducer,
	user: userReducer,
	news: newsReducer,
})