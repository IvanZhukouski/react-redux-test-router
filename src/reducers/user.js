const initialState = {
  id: +localStorage.getItem('userId'),
  isLoginRequest: false,
  isLogged: localStorage.getItem('loggedIn'),
}

export function userReducer(state = initialState, action) {
  switch (action.type) {
    
    case 'GET_LOGIN_REQUEST':
      return { ...state, isLogged: false, isLoginRequest: true}

    case 'GET_LOGIN_SUCCESS':
      return { ...state, isLogged: true, id: action.payload.data.id, isLoginRequest: false }

    case 'GET_LOGIN_ERROR':
      return { ...state, error: action.payload.message, isLogged: false, isLoginRequest: false }

    case 'REQUEST_ERROR':
      return { ...state, isLogged: false,data: [], isLoginRequest: false, requestError:  action.payload }

    case 'GET_DATA_SUCCESS':
      return { ...state, data: action.payload.data }

    case 'GET_LOGOUT_SUCCESS':
      return { ...state, isLogged: false, data: [], isLoginRequest: false, id: ''}

    default:
      return state
  }
}