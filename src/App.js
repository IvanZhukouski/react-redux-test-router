import React, {Component} from 'react';
import { connect } from 'react-redux'
import News from './containers/News'
import Books from './containers/Books'
import User from './containers/User.js'
import Home from './containers/Home'
import './App.css';
import {
  Route,
  NavLink,
  withRouter,
} from "react-router-dom";

class App extends Component {
  
  render(){
    return (
      <div className="App container">        
        <ul className="link-list">
          <NavLink exact to = '/'>Home</NavLink>
          <NavLink to = '/potter'>Potter</NavLink>
          <NavLink to = '/dark-tower'>Dark Tower</NavLink>
          <NavLink to = '/lord'>Lord</NavLink>
          <NavLink to = '/news'>News</NavLink>
          <NavLink to = '/profile'>Profile</NavLink>        
        </ul>
        
        <div className="content">
          <Route exact path = "/">
            <Home/>
          </Route>
          <Route path = "/potter">
            <Books bookName = {'potter'}/>
          </Route>

          <Route path = "/lord">
            <Books bookName = {'lord'}/>
          </Route>

          <Route path = "/dark-tower">
            <Books bookName = {'dark-tower'}/>
          </Route>

          <Route path = "/news" component = {News} />

          <Route exact path = '/profile' component = {User} />

          <Route path = '/profile/login' component = {User} />
        </div>

      </div>

    );
  }
}

const mapStateToProps = (store) =>{
  return {
    user: store.user,
  }
}

export default withRouter(connect(
  mapStateToProps,
)(App))
