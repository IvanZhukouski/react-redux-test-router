import React, {Component} from 'react';

class NewsData extends Component {
	renderNews(data){
		return(
			data.map((item) => {
				return <li key={item.id}>{item.text}</li>
			})
		)
	}
	render(){
		const{data, isFetching, error} = this.props;
		return(
			<div className="content">
				<h2>News</h2>
				<div className="news-container">
					{isFetching ? <img className = 'spinner' src="../spinner.png" alt=""/> : null}
					{data ? <ul>{this.renderNews(data)}</ul> : null}
				</div>
			</div>
			
		)
	}
}

export default NewsData;