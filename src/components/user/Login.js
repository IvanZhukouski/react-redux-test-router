import React, {Component} from 'react';

export default class Login extends Component {
	state = {
		email: '',
		password: '',

	}
	componentDidMount(){

	}
	onChangePassword = (event) => {
      this.setState({password: event.target.value});
    }

    onChangeLogin = (event) => {
      this.setState({email: event.target.value});
    }
	onSubmitLoginForm = (e, history) => {		
		e.preventDefault()
		const credentials = this.state
		const endpoint = 'https://mysterious-reef-29460.herokuapp.com/api/v1/validate';
		this.props.logIn(endpoint, credentials);
		this.setState({password:''});
		localStorage.setItem('loggedIn', 1)
	}

	render() {
		const {isLoginRequest, isLoginError, requestError} = this.props 
		return(
			<div className="logn">
				{(isLoginError) ? <p>Wrong email or password</p> : null	}
				{(requestError) ? <p>Some Error :{requestError} <br/> try later</p> : null}
				<h3>Please, log in </h3>
				<form action="#" onSubmit = {this.onSubmitLoginForm}>
					<div className="input-wrappper">
						<input required onChange = {this.onChangeLogin} type="email" name = 'email' value = {this.state.email}/>
						<input required onChange = {this.onChangePassword} type="password" name = 'password'  value={this.state.password}/>
						<button className ="login-button btn" > Log in </button>
					</div>
				</form>
				{(isLoginRequest) ? <img className = 'spinner' src="../spinner.png" alt=""/> : null}
			</div>

		)
	}
}
