import React, {Fragment} from 'react';

export default function UserInfo({ data }) {
  return (
    <div>
      <h1>Профиль</h1>
      {data && <ProfileContent data={data} />}

    </div>
  );
}


function ProfileContent({ data }) {
  return (
    <div>
      {
        data.city &&
        <p>Город: {data.city}</p>
      }

      {
        data.languages.length > 0 &&
        <Fragment>
          <p>Знание языков:</p>

          <ul>
            {data.languages.map(lang => (
              <li key={lang}>{lang}</li>
            ))}
          </ul>
        </Fragment>
      }

      {
        data.social.length > 0 &&
        <Fragment>
          <p>Ссылки:</p>

          <ul>
            {
              data.social
                .reduce((arr, socItem) => (
                  socItem.label === 'web' ? [socItem, ...arr] : [...arr, socItem] // перемещение 'web' в начало массива
                ), [])
                .map(socItem => (
                  <li key={socItem.label}>
                    <a href={socItem.link} target="_blank">
                      <img
                        src={`/icons/${socItem.label}.png`}
                        width="24"
                        height="24"
                        title={socItem.label}
                        alt={socItem.label}
                      />
                    </a>
                  </li>
                ))
            }
          </ul>
        </Fragment>
      }
    </div>
  );
}