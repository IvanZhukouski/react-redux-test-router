import React, {Component} from 'react';
import UserInfo from './UserInfo'

export default class Profile extends Component {
	
	logOut = () => {		
  		localStorage.removeItem('loggedIn');
  		localStorage.removeItem('userId');

  		this.props.logOutAction('/login')
	}
	componentDidMount(){
		const {getUserData, id} = this.props
		if(id){
			getUserData(id)
		}
	}

	render(){
		const {data} = this.props
		return(
			<div className = "profile-container">			
				<p>Profile Info</p>
				<button className="btn" onClick = {this.logOut}>Log Out</button>
				{ (data) ? <UserInfo data = {data} /> : null}
			</div>
		)
	}
}
