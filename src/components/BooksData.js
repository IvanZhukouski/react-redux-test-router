import React, {Component} from 'react';

class BooksData extends Component {
	render(){
		const{name, imgUrl, description} = this.props.data
		return(
			<div className = {'book-item'}>
				<h1>{name}</h1>
				<img src= {imgUrl} alt=""/>
				<p className="description">{description}</p>
			</div>
		)
	}
}
export default BooksData