export function logInAction(endpoint , credentials) {

	function userLogin(url = '', data = {}) {
        return fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(data), // тип данных в body должен соответвовать значению заголовка "Content-Type"
        })
    }

    

	return dispatch => { 
		
		dispatch({
			type: 'GET_LOGIN_REQUEST',
		})

		userLogin(endpoint, credentials)
	    	.then( response => {
		        if (!response.ok) {
		        	dispatch({
  						type: 'REQUEST_ERROR',
  						payload: response.status,
  					})
		        	throw Error(response.statusText);
		        	return response;
		        }
	      		return response.json();
		    })
		    .then(response => {
		    	
		    	if(response.status === 'ok'){						
	      			dispatch({
	      						type: 'GET_LOGIN_SUCCESS',
	      						payload: response,
	      					}
	      			)
	      			return response.data.id
	      		} else {
	      			dispatch({
	      						type: 'GET_LOGIN_ERROR',
	      						payload: response,
	      					}
	      			)	
	      		}
	      	})


	    .catch(error => console.error(error));
	    
	}
}
export function getUserData(id){
	
	function getData(userId){		
		return fetch('https://mysterious-reef-29460.herokuapp.com/api/v1/user-info/' + userId, {
            method: 'GET',
        })
    }
	return dispatch => {
		getData(id)
		.then(response => {
			if (!response.ok) {
	        	dispatch({
						type: 'GET_DATA_ERROR',
						payload: response.status,
					})
	        	throw Error(response.statusText);
	        	return response;
	        }
  			return response.json();
  		})
		.then(response =>{ localStorage.setItem('userId', id);
							dispatch({
		      						type: 'GET_DATA_SUCCESS',
		      						payload: response,
		      					}
		      				)
			      		}
		)

	}
}

export function logOut(url) {
  return {
    type: 'GET_LOGOUT_SUCCESS',
    payload: url,
  }
}