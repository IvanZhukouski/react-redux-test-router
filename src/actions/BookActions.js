export const getDataAction = endpoint => dispatch => {	//thunk creator
	
	dispatch({
		type: 'GET_DATA_REQUEST',
		payload: endpoint,
	})

	const getData = async endPoint => {

	    const API_ROOT = 'https://5ab9ca1ed9ac5c001434ecb4.mockapi.io'
	    try { 
	      const response = await fetch(`${API_ROOT}/${endPoint}`)
	      if (response.ok) {
	      	console.log('OK')
	        const json = await response.json()
	        return json
	      } else {
	        throw new Error(response.status) 
	      }
	    } catch (err) {
	      console.warn('httpGet error ', err)
	    }
	}
	getData(endpoint).then(
		json => {

			dispatch( {
						type: 'GET_DATA_SUCCESS',
						payload:json.books,
					}
			)
		}
	).catch(e => dispatch( {
						type: 'GET_DATA_ERROR',
						payload: new Error(e),
						error: true,
					}
		)
	)	

}