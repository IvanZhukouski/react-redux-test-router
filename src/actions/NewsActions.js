export const getNews = endpoint => dispatch =>{
	
	dispatch({
		type: 'GET_NEWS_DATA_REQUEST',
	})
	function getNewsData(endpoint){		
		return fetch(endpoint, {
            method: 'GET',
        })
    }
	getNewsData(endpoint)
	.then(response => {
		if (!response.ok) {
        	dispatch({
					type: 'GET_NEWS_DATA_ERROR',
					payload: response.status,
				})
        	throw Error(response.statusText);
        	return response;
        }
			return response.json();
		})
	.then(response =>{ console.log(response.data)
						dispatch({
	      						type: 'GET_NEWS_DATA_SUCCESS',
	      						payload: response.data,
	      					}
	      				)
		      		}
	)

} 