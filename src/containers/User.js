import React, {Component} from 'react';
import { connect } from 'react-redux';
import Login from '../components/user/Login';
import Profile from '../components/user/Profile';
import {logInAction, logOut, getUserData} from '../actions/UserActions';

class User extends Component {
	
	componentDidMount(){
		const {isLogged} = this.props.user;
		isLogged ? localStorage.setItem('loggedIn', true): localStorage.removeItem('loggedIn');
	}

	render() {
		console.log('user index render')
		const {isLogged, isLoginRequest, error, requestError, data, id } = this.props.user
		const {getUserData} = this.props
		if(isLogged){
			return (
				<div>
					<Profile logOutAction = {this.props.logOut}  id = {id} getUserData = {getUserData}  data = {data}/>
				</div>
			)
		} else return(
			<div>
				<Login logIn = {this.props.logIn} isLoginRequest = {isLoginRequest} isLoginError = {error} requestError = {requestError} />
			</div>
		)
	}
}

const mapStateToProps = (store) =>{
  return {
    user: store.user,
  }
}
const mapDispatchToProps = dispatch =>{
  return {
    logIn: (endpoint, credentials) => dispatch(logInAction(endpoint, credentials)),
    logOut: url => dispatch(logOut(url)),
    getUserData: id => dispatch(getUserData(id)),
  }
}
export default connect(
  mapStateToProps, mapDispatchToProps
)(User)
