import React, { Component } from 'react';
import NewsData from '../components/NewsData';
import { getNews } from '../actions/NewsActions'
import { connect } from 'react-redux'

class News extends Component{

	componentDidMount(){
		this.props.getNewsAction('https://mysterious-reef-29460.herokuapp.com/api/v1/news ')
	}
	render(){
		const {data, error, isFetching} = this.props.news;
		return(
			<NewsData data = {data} error= {error} isFetching = {isFetching}/>
		)
	}
}

const mapStateToProps = (store) => {
  return {
    news: store.news,
  }
}
const mapDispatchToProps = dispatch =>{
  return {
    getNewsAction: endpoint => dispatch(getNews(endpoint)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(News)