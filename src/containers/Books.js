import React, {Component} from 'react'
import BooksData from '../components/BooksData'
import{ getDataAction} from '../actions/BookActions'
import { connect } from 'react-redux'

class Books extends Component {

	componentDidMount() {
		const{bookName, getDataAction} = this.props
		this.props.getData(bookName)
	}

	renderBooks = (data) =>{
		if(data.length){
			return(
				data.map((item, index) => {
					return <BooksData data = {item} key = {index} />
				})
			)	
		} else return 'No data'
		
	}
	render() {

		const { isFetching, data, error } = this.props.books
		//console.log(this.props.books)
		console.log('book rendered')
		
		if( isFetching ){
			return <p>Loading...</p>

		} else if(error){
			return <b>{error}</b>

		} else{
			return this.renderBooks(data)
		}	
	}
}

const mapStateToProps = (store) => {
  return {
    books: store.books,
  }
}
const mapDispatchToProps = dispatch =>{
  return {
    getData: url => dispatch(getDataAction(url)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Books)